# CustomChatFastAPI
## Описание приложения
CustomChatFastAPI - приложение для простых приватных пользовательских чатов между двумя
пользователями. Оно также содержит возможность реализации уведомлений. Обмен сообщениями
между пользователями реализован с помощью сокетов. Для сохранения истории чатом и обмена
фотографиями используется REST API. Для авторизации и проверок прав доступа используются 
наработки из проекта [Код с README.md для авторизации](https://gitlab.com/newPsevdonim/customauthfastapi)
В нем подробно описано, как устроена авторизация и проверка пра доступа. Для проверки 
возможности обращения к фотографии реализован отельный роут, на который можно перенаправлять
nginx при возврате статики пользователю. 

#### Приложение содержит следующие роуты для авторизации:
* POST /auth/register - для регистрации новых пользователей 
* POST /auth/refresh - для обновления токена в случае если access_token не валиден
* POST /auth/login - для авторизации пользователя
* GET /auth/logout - для выхода из аккаунта
* GET /auth/reset-password-mail - для отправки сообщения на почту о сбросе пароля
* GET /auth/reset-password/{password_token} - для проверки валидности ссылки для смены пароля
* PUT /auth/reset_password - для замены пароля


### Приложение содержит следующие роуты для чатов\уведомлений:
* GET /chat/get_user_id
* PUT /chat/message_is_checked
* GER /chat/get_message_history
* GET /chat/get_notification
* PUT /chat/notifications_is_checked
* DELETE /chat/delete_notifications
* POST /chat/save_chat_photos

### Приложение содержит следующие роуты для проверки прав доступа к статике:
* GET /rights/static_files

## Установка библиотек и зависимостей при запуске с локального устройства без использования Docker-a
* Создать виртуальное окружение в директории проекта ```python -m venv venv```
* Активировать виртуальное окружение 
    Windows ```venv\Scripts\activate.bat```
    Linux ```source venv/bin/activate```
* Установить необходимые библиотеки с помощью команды ```pip install -r requirements.txt```
* Создать файл ```.env```  и задаем в нем следующие переменные
```
DB_HOST=<DB_HOST>
DB_PORT=<DB_PORT>
DB_NAME=<DB_NAME>
DB_USER=<DB_USER>
DB_PASS=<DB_PASS>

REDIS_HOST=<REDIS_HOST>
REDIS_PORT=<REDIS_PORT>

SECRET_KEY=<SECRET_KEY>
ACCESS_TOKEN_EXPIRE_MINUTES=<ACCESS_TOKEN_EXPIRE_MINUTES>
REFRESH_TOKEN_EXPIRE_DAYS=<REFRESH_TOKEN_EXPIRE_DAYS>
LOT_EXPIRE_DAYS=<LOT_EXPIRE_DAYS>
PASSWORD_TOKEN_EXPIRE_MINUTES=<PASSWORD_TOKEN_EXPIRE_MINUTES>

SMTP_USERNAME=<SMTP_USERNAME>
SMTP_PASSWORD=<SMTP_PASSWORD>
SMTP_EMAIL=<SMTP_EMAIL>
SMTP_PORT=<SMTP_PORT>

ABSOLUTE_CHATS_PATH=<ABSOLUTE_CHATS_PATH>

ADDRESS_API=<ADDRESS_API> #не забыть в конце про /
```
* Выполнить миграцию
* Запустить Redis ```sudo service redis-server start```
* Запустить приложение ```uvicorn app:app --reload```
* Перейти к [swagger](http://127.0.0.1:8000/docs) - сокет в нем не доступен

## Запуск приложения через docker-compose
* Добавить в файл ```.env``` переменную ```DB_LOCAL_HOST=localhost```
* Добавить в файл ```config.py``` следующие строчки кода:
```
DB_HOST_LOCAL = os.environ.get("DB_HOST_LOCAL")

if DB_HOST_LOCAL is None:
    raise Exception("The variable is not set: DB_HOST_LOCAL")
```
* Добавить в файл ```/migration/env.py``` импорт ```DB_LOCAL_HOST``` и строчку кода
```
config.set_section_option(section, "DB_HOST_LOCAL", DB_HOST_LOCAL)
```
* Изменить в файле ```alembic.ini``` переменную ```DB_HOST``` на ```DB_LOCAL_HOST```
* отредактировать файл ```/docker/docker-compouse``` изменив значения переменных на свои
```
    environment:
      - PGHOST=<PGHOST>
      - POSTGRES_DB=<POSTGRES_DB>
      - POSTGRES_USER=<POSTGRES_USER>
      - POSTGRES_PASSWORD=<POSTGRES_PASSWORD>
```
* Перейти из корневой директории в папку c ```docker-compouse```
```cd ./docker```
* Запустить docker-compose.yml ```docker-compose up --build -d```
* * Перейти к [swagger](http://127.0.0.1:8000/docs) после сборки контейнера - сокет в нем не доступен

## Создание бд и миграций
* Создать бд в postgres
* Прописать команду ```alembic init migration``` для создания конфигурационных файлов для выполнения миграции
* Отредактировать файлы ```alembic.ini, migrations/env.py``` (Этот и пункт выше нужно выполнять только если вы хотите использовать свою версию базы данных)
* Прописать ```alembic revision --autogenerate -m "Database creation"``` для создания скрипта миграций
* Прописать ```alembic upgrade {необходимые ключ из миграции}``` для обновления бд до определенной миграции

## Работа сокета
Для установки с сокетов нужно указать следующий путь ```ws://localhost:8000/chat/ws/{send_id}```.
Для получения ```send_id``` можно использовать метод ```GET /chat/get_user_id``` он 
возвращает следующий json ```{"id_owner": <send_id>}```. 

Сокет на вход принимает json следующих форматов:
```
# chat message
{
  "type_message": "chat",
  "get_id": <int value>,
  "message": <str value or None>,
  "photos_path": <List[str] or None>,
}

#notification message
{
   "type_message": "notification", 
   "get_id": <int value>,
   "message": <str value>,
   "type_notifications": <str value>
}
```

## Тестирование сокета 
Для тестирования работоспособности части кода отвечающего за передачу сообщений\уведомлений
были реализованы два приложения: ```/test/listen_message.py``` и ```/test/send_message.py```.
Они оба подключаются к сокету fast api. Первое прослушивает входящие сообщения, а второе 
отправляет сообщения с заданным интервалом. 
