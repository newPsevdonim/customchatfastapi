from pathlib import Path
import hashlib
import os

from fastapi import (
    Depends,
    Response,
    APIRouter,
    UploadFile,
    File,
    WebSocket,
    WebSocketDisconnect,
)
from fastapi import Request
from sqlalchemy import select, update, and_, or_, delete, desc
from sqlalchemy.ext.asyncio import AsyncSession

from config import ABSOLUTE_CHATS_PATH
from database import get_async_session
from src.auth.manager import check_user_rights
from src.chat.models import ChatHistory, NotificationsHistory
from src.chat.socket_manager import ConnectionManager

router = APIRouter(prefix="/chat", tags=["chat"])
connection_manager = ConnectionManager()


@router.get("/get_user_id")
async def get_user_id(
    response: Response,
    request: Request,
    session: AsyncSession = Depends(get_async_session),
):
    id_owner, user_email = check_user_rights(request, response)
    return {"id_owner": id_owner}


@router.put("/message_is_checked")
async def get_read_status(
    request: Request,
    response: Response,
    session: AsyncSession = Depends(get_async_session),
):
    id_owner, user_email = check_user_rights(request, response)
    query = (
        update(ChatHistory)
        .values(**{"is_read": True})
        .where(ChatHistory.send_id == id_owner)
    )
    await session.execute(query)
    await session.commit()
    await session.close()


@router.get("/get_message_history")
async def get_message_history(
    response: Response,
    request: Request,
    get_id: int,
    first: int,
    last: int,
    session: AsyncSession = Depends(get_async_session),
):
    id_owner, user_email = check_user_rights(request, response)
    print(get_id)
    print(id_owner)
    query = (
        select(ChatHistory)
        .filter(
            or_(
                and_(
                    ChatHistory.send_id == int(id_owner),
                    ChatHistory.get_id == int(get_id),
                ),
                and_(
                    ChatHistory.send_id == int(get_id),
                    ChatHistory.get_id == int(id_owner),
                ),
            )
        )
        .order_by(desc(ChatHistory.send_time))
        .offset(first)
        .limit(last - first + 1)
    )

    result = await session.execute(query)
    result = result.fetchall()
    data = []
    for index, value in enumerate(result):
        data.append(value[0].__dict__)

    return data


@router.get("/get_notifications")
async def get_notifications(
    request: Request,
    response: Response,
    first: int,
    last: int,
    session: AsyncSession = Depends(get_async_session),
):
    id_owner, user_email = check_user_rights(request, response)
    query = (
        select(NotificationsHistory)
        .filter(NotificationsHistory.user_id == id_owner)
        .filter(NotificationsHistory.is_read == True)
        .order_by(desc(NotificationsHistory.add_at))
        .offset(first)
        .limit(last - first + 1)
    )
    result = await session.execute(query)
    result = result.fetchall()
    data = []
    for index, value in enumerate(result):
        data.append(value[0].__dict__)

    return data


@router.put("/notifications_is_checked")
async def get_read_notifications(
    request: Request,
    response: Response,
    session: AsyncSession = Depends(get_async_session),
):
    id_owner, user_email = check_user_rights(request, response)
    values = {"is_read": True}
    query = (
        update(NotificationsHistory)
        .values(**values)
        .where(NotificationsHistory.send_id == id_owner)
    )
    await session.execute(query)
    await session.commit()
    await session.close()


@router.delete("/delete_notifications")
async def delete_notifications(
    request: Request,
    response: Response,
    id: int,
    session: AsyncSession = Depends(get_async_session),
):
    id_owner, user_email = check_user_rights(request, response)
    query = delete(NotificationsHistory).filter(NotificationsHistory.id == id)
    await session.execute(query)
    await session.commit()
    await session.close()


@router.post("/save_chat_photos")
async def save_chat_photos(
    response: Response,
    request: Request,
    get_id: int,
    files: list[UploadFile] = File(...),
    session: AsyncSession = Depends(get_async_session),
):
    id_owner, user_email = check_user_rights(request, response)
    first_path = os.path.join(
        ABSOLUTE_CHATS_PATH,
        hashlib.sha512(f"{id_owner}_{get_id}".encode("UTF-8")).hexdigest(),
    )
    second_path = os.path.join(
        ABSOLUTE_CHATS_PATH,
        hashlib.sha512(f"{get_id}_{id_owner}".encode("UTF-8")).hexdigest(),
    )
    directory_path_first = Path(first_path)
    directory_path_second = Path(second_path)
    path = None
    photo_count = None
    result = []
    if directory_path_first.is_dir():
        path = "first"
        photo_count = len(list(directory_path_first.glob("**/*.jpg")))
    elif directory_path_second.is_dir():
        path = "second"
        photo_count = len(list(directory_path_second.glob("**/*.jpg")))
    else:
        directory_path_first.mkdir(parents=True, exist_ok=True)
        path = "first"
        photo_count = len(list(directory_path_first.glob("**/*.jpg")))
    for index, file in enumerate(files):
        file_location = None
        if path == "first":
            file_location = os.path.join(first_path, f"{photo_count + index + 1}.jpg")
        else:
            file_location = os.path.join(second_path, f"{photo_count + index + 1}.jpg")
        with open(file_location, "wb") as file_object:
            file_object.write(file.file.read())
        result.append(file_location)
    return {"photos_path": result}


@router.websocket("/ws/{client_id}")
async def chat(websocket: WebSocket, client_id: int):
    await connection_manager.connect(websocket, client_id)
    try:
        while True:
            data = await websocket.receive_json()
            if data["type_message"] == "chat":
                await connection_manager.add_message_to_database(
                    send_id=client_id,
                    get_id=data["get_id"],
                    message=data["message"],
                    photos_path=data["photos_path"],
                )
                try:
                    await connection_manager.broadcast_personal_json(
                        send_id=client_id,
                        get_id=data["get_id"],
                        message=data["message"],
                        photos_path=data["photos_path"],
                        send_yourself=True,
                    )
                except KeyError:
                    await connection_manager.exit_user(
                        send_id=client_id, get_id=data["get_id"]
                    )
            elif data["type_message"] == "notification":
                await connection_manager.add_notifications_to_database(
                    get_id=data["get_id"],
                    message=data["message"],
                    type_notifications=data["type_notifications"],
                )
                try:
                    await connection_manager.broadcast_personal_notification(
                        get_id=data["get_id"],
                        message=data["message"],
                        type_notifications=data["type_notifications"],
                    )
                except KeyError:
                    await connection_manager.exit_user(
                        send_id=client_id, get_id=data["get_id"]
                    )
    except WebSocketDisconnect:
        connection_manager.disconnect(client_id)
        await connection_manager.broadcast(client_id=client_id)
