from datetime import datetime
from typing import List

from fastapi import WebSocket
from sqlalchemy import insert

from src.chat.models import ChatHistory, NotificationsHistory
from src.chat.sheme import InsertChatHistory, InsertNotificationHistory
from database import async_session_maker


class ConnectionManager:
    def __init__(self):
        self.active_connections: dict[int, WebSocket] = {}

    async def connect(self, websocket: WebSocket, client_id: int):
        await websocket.accept()
        self.active_connections[client_id] = websocket

    def disconnect(self, client_id: int):
        self.active_connections.pop(client_id, "No Key found")

    async def broadcast(self, client_id: int):
        for connection in self.active_connections.keys():
            await self.active_connections[connection].send_json(
                {"client_id": client_id, "message": "exit"}
            )

    async def exit_user(self, send_id, get_id):
        await self.active_connections[send_id].send_json(
            {"get_id": get_id, "message": "exit"}
        )

    async def broadcast_personal_json(
        self,
        send_id: int,
        get_id: int,
        message: str = None,
        send_yourself: bool = False,
        photos_path: List[str] = None,
    ):
        await self.add_message_to_database(
            send_id=send_id, get_id=get_id, message=message
        )
        message_data = {
            "message": message,
            "send_id": send_id,
            "photos_path": photos_path,
        }
        if send_yourself:
            await self.active_connections[send_id].send_json(message_data)
        await self.active_connections[get_id].send_json(message_data)

    async def broadcast_personal_notification(
        self, get_id: int, message: str, type_notifications: str
    ):
        notification_id = await self.add_notifications_to_database(
            get_id=get_id, message=message, type_notifications=type_notifications
        )
        values = {
            "notification_id": notification_id,
            "get_id": get_id,
            "message": str,
            "type_notifications": type_notifications,
            "datetime": datetime.now(),
        }
        await self.active_connections[get_id].send_json({**values})

    @staticmethod
    async def broadcast_user_exit(websocket: WebSocket):
        await websocket.send_json({"status": "user exit"})

    @staticmethod
    async def add_message_to_database(
        send_id: int, get_id: int, message: str = None, photos_path: List[str] = None
    ):
        async with async_session_maker() as session:
            smtp = insert(ChatHistory).values(
                **InsertChatHistory(
                    **{
                        "send_id": send_id,
                        "get_id": get_id,
                        "message": message,
                        "photos_path": photos_path,
                    }
                ).dict()
            )
            await session.execute(smtp)
            await session.commit()

    @staticmethod
    async def add_notifications_to_database(
        get_id: int, message: str, type_notifications: str
    ):
        async with async_session_maker() as session:
            smtp = insert(NotificationsHistory).values(
                **InsertNotificationHistory(
                    **{
                        "user_id": get_id,
                        "type_notifications": type_notifications,
                        "message": message,
                    }
                ).dict()
            )
            await session.execute(smtp)
            await session.commit()
