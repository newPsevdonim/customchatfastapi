from typing import List

from pydantic import BaseModel


class InsertChatHistory(BaseModel):
    send_id: int
    get_id: int
    message: str = None
    photos_path: List[str] = None

    class Config:
        validate_assignment = True


class InsertNotificationHistory(BaseModel):
    user_id: int
    type_notifications: str
    message: str

    class Config:
        validate_assignment = True
