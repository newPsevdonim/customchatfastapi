from sqlalchemy import (
    Column,
    Integer,
    String,
    DateTime,
    BOOLEAN,
)
from sqlalchemy.dialects.postgresql import ARRAY
from datetime import datetime

from database import Base


class ChatHistory(Base):
    __tablename__ = "ChatHistory"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=True)
    send_id = Column(Integer)
    get_id = Column(Integer)
    message = Column(String)
    send_time = Column(DateTime, default=datetime.utcnow())
    photos_path = Column(ARRAY(String), nullable=True)
    is_read = Column(BOOLEAN, default=False)

    def __repr__(self):
        return (
            f"<ChatPhotos(id='{self.id}',"
            f" send_id='{self.send_id},"
            f" get_id='{self.get_id}',"
            f" message='{self.message}',"
            f" send_time='{self.send_time}',"
            f" photos_path='{self.photos_path}',"
            f" is_read='{self.is_read}')>"
        )


class NotificationsHistory(Base):
    __tablename__ = "NotificationsHistory"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=True)
    user_id = Column(Integer)
    type_notifications = Column(String)
    add_at = Column(DateTime, default=datetime.utcnow())
    message = Column(String)
    is_read = Column(BOOLEAN, default=False)

    def __repr__(self):
        return (
            f"<NotificationsHistory(id='{self.id}',"
            f" user_id='{self.user_id},"
            f" type_notification='{self.type_notifications}',"
            f" add_at='{self.add_at}',"
            f" message='{self.message}',"
            f" is_read='{self.is_read}')>"
        )
