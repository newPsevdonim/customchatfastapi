from fastapi import (
    Depends,
    Response,
    APIRouter,
)
from fastapi import Request
from sqlalchemy import select, or_
from sqlalchemy.ext.asyncio import AsyncSession

from database import get_async_session
from src.auth.manager import check_user_rights
from src.chat.models import ChatHistory

router = APIRouter(prefix="/rights", tags=["rights"])


@router.get("/statics_file")
async def get_user_id(
    response: Response,
    request: Request,
    photos_path: str,
    session: AsyncSession = Depends(get_async_session),
):
    id_owner, user_email = check_user_rights(request, response)
    query = (
        select(ChatHistory)
        .filter(or_(ChatHistory.send_id == id_owner, ChatHistory.get_id == id_owner))
        .filter(ChatHistory.categories.contains([photos_path]))
        )
    result = await session.execute(query)
    if not result.scalar():
        return {"status": "denied"}
    else:
        return {"status": "allowed"}
