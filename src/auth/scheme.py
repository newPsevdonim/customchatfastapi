from pydantic import BaseModel


class UserCreate(BaseModel):
    login: str
    password: str
    mail: str


class UserLogin(BaseModel):
    login_email: str
    password: str
