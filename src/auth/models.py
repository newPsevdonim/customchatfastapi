from datetime import datetime

from sqlalchemy import (
    Column,
    Integer,
    String,
    DateTime,
    BOOLEAN,
)

from database import Base


class User(Base):
    __tablename__ = "User"

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=True)
    login = Column(String)
    password = Column(String)
    mail = Column(String)
    rights = Column(String, default="user")
    register_at = Column(DateTime, default=datetime.utcnow())
    rights_end = Column(DateTime, default=datetime.utcnow())
    is_admin = Column(BOOLEAN, default=False)
    disabled = Column(BOOLEAN, default=False)

    def __repr__(self):
        return (
            f"<User(id={self.id},"
            f" login='{self.login}',"
            f" email='{self.mail}',"
            f" rights='{self.rights},"
            f" register_at='{self.register_at}',"
            f" rights_end='{self.rights_end}',"
            f" is_admin='{self.is_admin}',"
            f" disabled='{self.disabled}')>"
        )
