import jwt
from sqlalchemy import select
from validate_email import validate_email

from datetime import datetime, timedelta
import re

from fastapi import HTTPException

from config import (
    ACCESS_TOKEN_EXPIRE_MINUTES,
    REFRESH_TOKEN_EXPIRE_DAYS,
    SECRET_KEY,
    PASSWORD_TOKEN_EXPIRE_MINUTES,
)

from src.auth.models import User


def generate_tokens(login, id, email):
    access_token_expires = datetime.utcnow() + timedelta(
        minutes=int(ACCESS_TOKEN_EXPIRE_MINUTES)
    )
    access_token_payload = {
        "login": login,
        "id": id,
        "email": email,
        "exp": access_token_expires,
        "type": "access",
        "app": "rec games",
    }
    access_token = jwt.encode(access_token_payload, SECRET_KEY, algorithm="HS256")

    refresh_token_expires = datetime.utcnow() + timedelta(
        days=int(REFRESH_TOKEN_EXPIRE_DAYS)
    )
    refresh_token_payload = {
        "login": login,
        "id": id,
        "email": email,
        "exp": refresh_token_expires,
        "type": "refresh",
        "app": "rec games",
    }
    refresh_token = jwt.encode(refresh_token_payload, SECRET_KEY, algorithm="HS256")

    return access_token, refresh_token


# функция для генерации токена смены пароля
def generate_password_token(email):
    password_token_expires = datetime.utcnow() + timedelta(
        minutes=int(PASSWORD_TOKEN_EXPIRE_MINUTES)
    )
    password_token_payload = {
        "email": email,
        "exp": password_token_expires,
        "type": "password",
        "app": "rec games",
    }
    password_token = jwt.encode(password_token_payload, SECRET_KEY, algorithm="HS256")
    return password_token


# функция для проверки валидности токена смены пароля
def is_password_refresh_token_valid(token):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
    except jwt.ExpiredSignatureError:
        return False
    except (jwt.InvalidTokenError, KeyError):
        return False

    if payload["type"] != "password":
        return False

    return datetime.utcnow() < datetime.fromtimestamp(payload["exp"])


# функция для проверки валидности токена доступа
def is_access_token_valid(token):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
    except jwt.ExpiredSignatureError:
        return False
    except (jwt.InvalidTokenError, KeyError):
        return False

    if payload["type"] != "access":
        return False

    return datetime.utcnow() < datetime.fromtimestamp(payload["exp"])


def get_user_id_from_token(token: str):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
    except jwt.ExpiredSignatureError:
        return False
    except (jwt.InvalidTokenError, KeyError):
        return False

    return payload["id"]


def get_user_login_from_token(token: str):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
    except jwt.ExpiredSignatureError:
        return False
    except (jwt.InvalidTokenError, KeyError):
        return False

    return payload["login"]


def get_user_email_from_token(token):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
    except jwt.ExpiredSignatureError:
        return False
    except (jwt.InvalidTokenError, KeyError):
        return False

    return payload["email"]


# функция для проверки валидности токена обновления
def is_refresh_token_valid(token):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
    except jwt.ExpiredSignatureError:
        return False
    except (jwt.InvalidTokenError, KeyError):
        return False

    if payload["type"] != "refresh":
        return False

    return datetime.utcnow() < datetime.fromtimestamp(payload["exp"])


# функция для обновления токена доступа
def refresh_access_token(refresh_token):
    try:
        payload = jwt.decode(refresh_token, SECRET_KEY, algorithms=["HS256"])
        access_token, _ = generate_tokens(
            payload["login"], payload["id"], payload["email"]
        )
        return access_token
    except (jwt.InvalidTokenError, KeyError):
        return None


# извлечение данных из токена для смены пароля
def take_token_data(token):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
        return payload["email"]
    except jwt.ExpiredSignatureError:
        return None
    except (jwt.InvalidTokenError, KeyError):
        return None


# функция для проверки валидности почты
def is_valid_email(email_address):
    is_valid = validate_email(email_address, debug=True)
    return is_valid


# функция для проверки валидности пароля
def is_valid_password(password):
    if len(password) < 8:
        return False
    elif re.search("[0-9]", password) is None:
        return False
    elif re.search("[A-Z]", password) is None:
        return False
    elif re.search("[a-z]", password) is None:
        return False
    elif re.search(r"[!@#?_=+$%^&*(),./|]", password) is None:
        return False
    else:
        return True


def check_user_rights(request, response, use_login=False):
    access_token = request.cookies.get("access_token")
    refresh_token = request.cookies.get("refresh_token")

    if access_token is None or refresh_token is None:
        raise HTTPException(
            status_code=403,
            detail={
                "status": "error",
                "data": f"not found access_token or refresh_token",
                "details": "not found access_token or refresh_token",
                "error_details": "not found access_token or refresh_token",
            },
        )
    if is_access_token_valid(access_token) is False:
        if not refresh_token:
            raise HTTPException(
                status_code=401,
                detail={
                    "status": "error",
                    "data": f"Invalid refresh token",
                    "details": "Invalid refresh token",
                    "error_details": "Invalid refresh token",
                },
            )
        if is_refresh_token_valid(refresh_token) is False:
            raise HTTPException(
                status_code=401,
                detail={
                    "status": "error",
                    "data": f"Invalid refresh token",
                    "details": "Invalid refresh token",
                    "error_details": "Invalid refresh token",
                },
            )
        access_token = refresh_access_token(refresh_token)
        response.set_cookie(
            key="access_token",
            value=access_token,
            httponly=True,
            max_age=ACCESS_TOKEN_EXPIRE_MINUTES * 60,
        )
    id_owner = get_user_id_from_token(access_token)
    user_email = get_user_email_from_token(access_token)
    if use_login:
        login = get_user_login_from_token(access_token)
        return id_owner, user_email, login
    else:
        return id_owner, user_email


async def check_admins(
    request, response, session, rights_list, use_login=False, return_rights=False
):
    id_owner, user_email = check_user_rights(request, response)
    query = select(User.is_admin, User.rights).where(User.id == id_owner)
    result = await session.execute(query)
    result = result.fetchall()[0]
    is_admin, rights = result[0], result[1]
    if return_rights:
        return id_owner, rights
    if rights in rights_list:
        return id_owner
    else:
        raise HTTPException(
            status_code=403,
            detail={
                "status": "error",
                "data": f"User haven't admins right",
                "details": "User haven't admins right",
                "error_details": "User haven't admins right",
            },
        )
