#!/bin/bash


cd ~
postgres -c log_statement=all
initdb -d /var/lib/postgresql/data/
pg_ctl -D /var/lib/postgresql/data/ -l ./db_log start
createdb $DB_NAME

cd /database
cp pg_hba.conf /var/lib/postgresql/data/
pg_ctl reload

alembic upgrade head
