import asyncio
import websockets


async def receive_messages():
    uri = "ws://localhost:8000/chat/ws/2"  # Замените на ваш URL сокета

    async with websockets.connect(uri) as websocket:
        while True:
            message = await websocket.recv()
            print(f"Received message: {message}")


asyncio.get_event_loop().run_until_complete(receive_messages())
