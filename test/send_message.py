import asyncio
import websockets
import json
import time


async def send_messages():
    uri = "ws://localhost:8000/chat/ws/1"  # Замените на ваш URL сокета

    async with websockets.connect(uri) as websocket:
        for i in range(5):  # Отправляем 5 сообщений
            data = {
                "type_message": "chat",
                "get_id": 1,
                "message": f"Message {i+1}",
                "photos_path": None,
            }
            message = json.dumps(data)
            await websocket.send(message)
            print(f"Sent message: {message}")
            # Ждем 1 секунду перед отправкой следующего сообщения
            await asyncio.sleep(10)


asyncio.get_event_loop().run_until_complete(send_messages())
